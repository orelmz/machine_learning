setwd("C:/Users/user/Desktop/hwfiles")
bikes.data.raw <- read.csv("����� ��� ������ ����� - ����� ����� ������� -    278090.csv")

str(bikes.data.raw)
library(ggplot2)

bikes.data.prepared <- bikes.data.raw

#turn date time into character
bikes.data.prepared$datetime <- as.character(bikes.data.prepared$datetime)
str(bikes.data.prepared)


Sys.setlocale("LC_TIME","English")
date <- substr(bikes.data.prepared$datetime, 1, 10)
days <- weekdays(as.Date(date))

#Add weekdays
bikes.data.prepared$weekday <- as.factor(days)

str(bikes.data.prepared)

#Add month
month <- as.integer(substr(date , 6, 7))

bikes.data.prepared$month <- month

#Add years
year <- as.integer(substr(date, 1,4))
bikes.data.prepared$year <- year

#Add hour
hour <- as.integer(substr(bikes.data.prepared$datetime, 12,13))
bikes.data.prepared$hour <- hour

#Find seasons
table(bikes.data.prepared$season, bikes.data.prepared$month)

#Turn season into factor
bikes.data.prepared$season <- factor(bikes.data.prepared$season, levels = 1:4, labels = c('winter','spring','summer','automn'))


#-------------------------------------------------------------------------------------------------------------------
#Turn weather into factor
weather <- c('clear' , 'cloudy' , 'light rain' , 'heavy rain')
bikes.data.prepared$weather<- factor(bikes.data.prepared$weather , levels = 1:4 , labels = weather)


#install.packages('ggplot2')
#library(ggplot2)

#how seasons affacts count
ggplot(bikes.data.prepared , aes(x = season ,y = count,color = season)) + geom_boxplot()

#making graph called boxplot
#how weather affacts count
ggplot(bikes.data.prepared , aes(x = weather , ,y = count,color = weather)) + geom_boxplot()

#look at time impact
ggplot(bikes.data.prepared, aes(as.factor(year),count)) +geom_boxplot()
ggplot(bikes.data.prepared, aes(as.factor(month),count)) +geom_boxplot()
ggplot(bikes.data.prepared, aes(days,count)) +geom_boxplot()
ggplot(bikes.data.prepared, aes(as.factor(hour),count)) +geom_boxplot()

#bininig hour
breaks <- c(0,7,8,9,17,20,21,22,24)
labels <- c("0-6","7","8","9-16","17-19","20","21","22-23")
bins <- cut(bikes.data.prepared$hour, breaks, include.lowest = T, right = F, lables = lables)


#Add to datafram
bikes.data.prepared$hourb <- bins
str(bikes.data.prepared)
ggplot(bikes.data.prepared, aes(hourb,count)) +geom_boxplot()


#How temp affect count
ggplot(bikes.data.prepared, aes(temp ,count)) +geom_point() + stat_smooth(method = lm)

#How wind affect count
ggplot(bikes.data.prepared, aes(windspeed ,count)) +geom_point() + stat_smooth(method = lm)

str(bikes.data.prepared)

bikes.data.final <- bikes.data.prepared

bikes.data.final$datetime <-NULL 
bikes.data.final$season <-NULL 
bikes.data.final$atemp <-NULL 
bikes.data.final$casual <-NULL 
bikes.data.final$registered <-NULL 
bikes.data.final$hour <-NULL 

str(bikes.data.final)

#install.packages("caTools")
library(caTools)

filter <- sample.split(bikes.data.final$humidity, SplitRatio = 0.7)
str